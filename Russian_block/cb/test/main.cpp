#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<conio.h>

#define SPACE 32
#define LEFT 75
#define RIGHT 77
#define DOWN 80
#define ESC 27
#define Wall 2
#define Box 1
#define Kong 0
#define FACE_X 20 //界面尺寸------水平
#define FACE_Y 29  //界面尺寸------垂直
void gotoxy(int x,int y) //移动坐标
{
    COORD c;
    c.X=y;
    c.Y=x;
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), c );
}
void hid_cursor()//隐藏光标
{
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO cursor;
    GetConsoleCursorInfo(hOut,&cursor);
    cursor.bVisible=0;//1为显示，0为隐藏
    SetConsoleCursorInfo(hOut,&cursor);
}

//void gotoxy(int x,int y) //移动坐标
//int color(int c);   //颜色
//void hid_cursor();  //隐藏光标
void inter_face();  //初始化界面
void init_dia();   //初始化方块信息
void draw_dia(int base,int space_c,int x,int y);   //覆盖方块
void draw_kong(int base,int space_c,int x,int y); //画方块
int pd(int n,int space_c,int x,int y);  //判断是否到底
void start_game();   //开始游戏
int xc();//消除
//void read_file();   //读写最高记录
//void write_file();  //写最高纪录
int grade=0;//当前分数
//int max=0;//最高记录
int nn=0;

struct Face
{
    int data[FACE_Y][FACE_X+10];    //数值，为1是方块，为0是空格
    //  int color[FACE_X][FACE_Y+10];   //对应方块的颜色
} face;
typedef struct Diamonds
{
    int space[4][4];        //4*4矩阵，为1为方块，为0 为空
} Dia;
Dia dia[7][4];  //一维基础7个方块，二维表示旋转次数
int  main()
{
    system("cls");
    system("title 俄罗斯方块");
    system("mode con cols=60 lines=40");  //窗口宽度高度
    hid_cursor();
    srand(time(NULL));
    grade=0;
    inter_face();
    init_dia();
    nn=rand()%7;
    while(1)
        start_game();
    return 0;
}
void inter_face()//界面
{
    int i,j;
    for (j=0; j<FACE_Y; j++)
        for (i=0; i<FACE_X+10; i++)
        {
            if(i==0||i==FACE_X-1||i==FACE_X+9)
            {
                face.data[j][i]=Wall;
                gotoxy(j,2*i);
                printf("▓");
            }
            else if (j==FACE_Y-1)
            {
                face.data[j][i]=Box;
                gotoxy(j,2*i);
                printf("▓");
            }
            else
                face.data[j][i]=Kong;
        }
    gotoxy(FACE_Y-26,2*FACE_X+2);
    printf("next:");

    gotoxy(FACE_Y-18,2*FACE_X+2);
    printf("左移：←");

    gotoxy(FACE_Y-16,2*FACE_X+2);
    printf("右移：→");

    gotoxy(FACE_Y-14,2*FACE_X+2);
    printf("旋转：space");

    gotoxy(FACE_Y-12,2*FACE_X+2);
    printf("暂停: S");

    gotoxy(FACE_Y-10,2*FACE_X+2);
    printf("退出: ESC");

    gotoxy(FACE_Y-8,2*FACE_X+2);
    printf("重新开始:R");

//    gotoxy(FACE_Y-6,2*FACE_X+2);
//   printf("最高纪录:%d",max);

    gotoxy(FACE_Y-4,2*FACE_X+2);
    printf("分数：%d",grade);
}
void init_dia()
{
    int i,j,k,z;
    int tmp[4][4];
    for(i=0; i<3; i++)
        dia[0][0].space[1][i]=1;
    dia[0][0].space[2][1]=1;     //土形

    for(i=1; i<4; i++)
        dia[1][0].space[i][1]=1;
    dia[1][0].space[1][2]=1;    //L形--1

    for(i=1; i<4; i++)
        dia[2][0].space[i][2]=1;
    dia[2][0].space[1][1]=1;    //L形--2

    for(i=0; i<2; i++)
    {
        dia[3][0].space[1][i]=1;
        dia[3][0].space[2][i+1]=1; //Z形--1

        dia[4][0].space[1][i+1]=1;
        dia[4][0].space[2][i]=1;//Z形--2

        dia[5][0].space[1][i+1]=1;
        dia[5][0].space[2][i+1]=1;//田字形
    }

    for(i=0; i<4; i++)
        dia[6][0].space[i][2]=1;//I形

    for(i=0; i<7; i++)
    {
        for(z=0; z<3; z++)
        {
            for(j=0; j<4; j++)
            {
                for(k=0; k<4; k++)
                {
                    tmp[j][k]=dia[i][z].space[j][k];
                }
            }
            for(j=0; j<4; j++)
            {
                for(k=0; k<4; k++)
                {
                    dia[i][z+1].space[j][k]=tmp[4-k-1][j];
                }
            }
        }
    }
    ///////////旋转后21个图形
}

void draw_dia(int base,int space_c,int x,int y)
{
    int i,j;
    for(i=0; i<4; i++)
    {
        for(j=0; j<4; j++)
        {
            gotoxy(x+i,2*(y+j));
            if(dia[base][space_c].space[i][j]==1)
                printf("■");
        }
    }
}

void draw_kong(int base,int space_c,int x,int y)
{
    int i,j;
    for(i=0; i<4; i++)
    {
        for(j=0; j<4; j++)
        {
            gotoxy(x+i,2*(y+j));
            if(dia[base][space_c].space[i][j]==1)
                printf("  ");
        }
    }

}

int pd(int n,int space_c,int x,int y)   //判断是否到底
{
    int i,j;
    for(i=0; i<4; i++)
    {
        for(j=0; j<4; j++)
        {
            if(dia[n][space_c].space[i][j]==0)
                continue;
            else if(face.data[x+i][y+j]==Wall || face.data[x+i][y+j]==Box)
                return 0;
        }
    }
    return 1;
}

int xc()//消除
{
    int i,j,k,sum;
    for(i=FACE_Y-2; i>4; i--)
    {
        sum=0;
        for(j=1; j<FACE_X-1; j++)
        {
            sum+=face.data[i][j];
        }
        if(sum==0)
            break;
        if(sum==FACE_X-2)   //满一行，减掉
        {
            grade+=100;
            //     color(7);
            gotoxy(FACE_Y-4,2*FACE_X+2);
            printf("分数：%d",grade);
            for(j=1; j<FACE_X-1; j++)
            {
                face.data[i][j]=Kong;
                gotoxy(i,2*j);
                printf(" ");
            }
            for(j=i; j>1; j--)
            {
                sum=0;
                for(k=1; k<FACE_X-1; k++)
                {
                    sum+=face.data[j-1][k]+face.data[j][k];
                    face.data[j][k]=face.data[j-1][k];
                    if(face.data[j][k]==Kong)
                    {
                        gotoxy(j,2*k);
                        printf(" ");
                    }
                    else
                    {
                        gotoxy(j,2*k);
                        //     color(face.color[j][k]);
                        printf("■");
                    }
                }
                if(sum==0)
                    return 1;
            }

        }
    }
    for(i=1; i<FACE_X-1; i++)
    {
        if(face.data[1][i]==Box)
        {
            char n;
            Sleep(2000); //延时
            system("cls");
            gotoxy(FACE_Y/2,2*(FACE_X/3));
            printf("GAME OVER!\n");
            do
            {
                gotoxy(FACE_Y/2+2,2*(FACE_X/3));
                printf("是否重新开始游戏(y/n): ");
                scanf("%c",&n);
                gotoxy(FACE_Y/2+4,2*(FACE_X/3));
                if(n!='n' && n!='N' && n!='y' && n!='Y')
                    printf("输入错误，请重新输入!");
                else
                    break;
            }
            while(1);
            if(n=='n' || n=='N')
            {
                gotoxy(FACE_Y/2+4,2*(FACE_X/3));
                printf("按任意键退出游戏！");
                exit(0);
            }
            else if(n=='y' || n=='Y')
                main();
        }
    }
    return 0;
}

void start_game()
{
    int n,ch,t=0,x=0,y=FACE_X/2-2,i,j;
    int space_c=0;//旋转次数
    draw_kong(nn,space_c,4,FACE_X+3);
    n=nn;
    nn=rand()%7;    //随机生成下一块
    //color(nn);
    draw_dia(nn,space_c,4,FACE_X+3);
    while(1)
    {
        //   color(n);
        draw_dia(n,space_c,x,y);//画出图形
        if(t==0)
            t=15000;
        while(--t)
        {
            if(kbhit()!=0)//有输入就跳出
                break;
        }
        if(t==0)
        {
            if(pd(n,space_c,x+1,y)==1)
            {
                draw_kong(n,space_c,x,y);
                x++;            //向下降落
            }
            else
            {
                for(i=0; i<4; i++)
                {
                    for(j=0; j<4; j++)
                    {
                        if(dia[n][space_c].space[i][j]==1)
                        {
                            face.data[x+i][y+j]=Box;
                            // face.color[x+i][y+j]=n;
                            while(xc());
                        }
                    }
                }
                return;
            }
        }
        else
        {
            ch=getch();
            switch(ch)     //移动
            {
            case LEFT:
                if(pd(n,space_c,x,y-1)==1)   //判断是否可以移动
                {
                    draw_kong(n,space_c,x,y);
                    y--;
                }
                break;
            case RIGHT:
                if(pd(n,space_c,x,y+1)==1)
                {
                    draw_kong(n,space_c,x,y);
                    y++;
                }
                break;
            case DOWN:
                if(pd(n,space_c,x+1,y)==1)
                {
                    draw_kong(n,space_c,x,y);
                    x++;
                }
                break;
            case SPACE:
                if(pd(n,(space_c+1)%4,x+1,y)==1)
                {
                    draw_kong(n,space_c,x,y);
                    space_c=(space_c+1)%4;
                }
                break;
            case ESC:
                system("cls");
                gotoxy(FACE_Y/2,FACE_X);
                printf("---游戏结束!---\n\n");
                gotoxy(FACE_Y/2+2,FACE_X);
                printf("---按任意键退出!---\n");
                getch();
                exit(0);
                break;
            case  'R':
            case  'r':
                main();
                exit(0);
            case  'S':
            case  's':
                while(1)
                {
                    if(kbhit()!=0)//有输入就跳出
                        break;
                }
                break;
            }
        }
    }
}
